import IRoute from '../interface/route'
import HomePage from '../pages/home_page'
import LoginPage from '../pages/login_page'

const routes: IRoute[] = [
    {
        path: "/",
        name: "LoginPage",
        component: LoginPage,
        exact: true,
    },
    {
        path: "/login",
        name: "LoginPage",
        component: LoginPage,
        exact: true
    },
    {
        path: "/home",
        name: "HomePage",
        component: HomePage,
        exact: true
    },
]

export default routes
