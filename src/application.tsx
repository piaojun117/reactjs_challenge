import React, { useEffect } from 'react'
import { BrowserRouter, Route, Switch, RouteComponentProps } from 'react-router-dom'
import routes from './config/routes'
import './App.css';
import { Provider } from 'react-redux'
import store from './redux/store'

const Application: React.FunctionComponent<{}> = props => {

    useEffect(() => {
        console.log("Application Logger")
    }, [])

    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    {routes.map((route, index) => {
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                render={(props: RouteComponentProps<any>) => (
                                    <route.component
                                        name={route.name}
                                        {...props}
                                        {...route.props}
                                    />
                                )}
                            />
                        )
                    })}
                </Switch>
            </BrowserRouter>
        </Provider>
    )

}

export default Application