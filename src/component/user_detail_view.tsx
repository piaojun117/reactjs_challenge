import React, { PropsWithChildren, useEffect } from 'react'
import { connect } from 'react-redux'

const UserDetailView: React.FunctionComponent<PropsWithChildren<any>> = ({ user_data }) => {
    return (
        <div className="UserDetailTitle">
            <span className="UserNameText">{user_data.name}</span>
            <span className="UserIDText">{user_data.number === "" ? "" : "#" + user_data.number}</span>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    // console.log("state", state)
    return {
        user_data: state.user_data
    }
}

export default connect(mapStateToProps)(UserDetailView)