import React, { PropsWithChildren } from 'react'

const PageButton: React.FunctionComponent<PropsWithChildren<any>> = (props) => {

    return (
        <button
            className={(props.is_selected) ? "SelectedPageNumberButton" : "PageNumberButton"}
            onClick={() => props.onClick(props.page_number)}
        >{props.page_number}</button>
    )
}

export default PageButton