import React, { PropsWithChildren } from 'react'

const NextPrevPageButton: React.FunctionComponent<PropsWithChildren<any>> = (props) => {

    function onClickPageButton() {
        console.log(props.name)
    }

    return (
        <div className="NextPrevPageButton" onClick={() => onClickPageButton()}>
            <span className="PageButtonName">{props.name}</span>
        </div>
    )
}

export default NextPrevPageButton