import React, { PropsWithChildren } from 'react'
import UserData from '../interface/user_data'
import { connect } from 'react-redux'
import onSelectUser from '../redux/action'

const UserListView: React.FunctionComponent<PropsWithChildren<any>> = ({ page_items, onSelectUser }) => {

    return (
        page_items.map((item: UserData) => (
            <div key={item.id} className="UserListItem" onClick={() => onSelectUser(item)}>
                <img src={item.image} alt="Avatar" className="Avatar" />
                <span className="UserNumber">{item.number}</span>
                <span className="UserName">{item.name}</span>
            </div>))
    )
}

const mapStateToProps = (state: any) => {
    // console.log("state", state)
    return {
        user_data: state.user_data
    }
}

const mapDispatchToProps = {
    onSelectUser: (user_data: UserData) => onSelectUser(user_data)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserListView)