
export default interface UserData {
    id: string; // "UG9rZW1vbjowMDE=",
    number: string; //"001",
    name: string; //"Bulbasaur",
    image?: string; //"https://img.pokemondb.net/artwork/bulbasaur.jpg",
    classification?: string; //"Seed Pokémon",
    weight?: {
        minimum: string; //"6.04kg",
        maximum: string; //"7.76kg"
    },
    height?: {
        minimum: string; //"0.61m",
        maximum: string; //"0.79m"
    }
}