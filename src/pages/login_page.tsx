import React, { FormEvent, PropsWithChildren, useState } from 'react'
import { Redirect } from 'react-router'
import IPage from '../interface/pages'

const LoginPage: React.FunctionComponent<IPage & PropsWithChildren<any>> = (props) => {

    console.log(props)

    const sendForm = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const { email, password } = event.target as typeof event.target & {
            email: { value: string },
            password: { value: string },
        }
        console.log(email.value, ":", password.value)
        analyze(password.value)
    }

    const analyze = (password: string) => {
        let mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        if (!mediumRegex.test(password)) {
            console.log("Weak Password");
        }
        props.history.replace("/home")
    }

    return (
        <div className="LoginFormContainer">
            <form className="LoginForm" onSubmit={(event) => sendForm(event)}>
                <input
                    id="email"
                    type="email"
                    placeholder="Email"
                    required
                    maxLength={30}
                    className="InputEmail" />
                <input
                    id="password"
                    type="password"
                    placeholder="Password"
                    maxLength={20}
                    minLength={6}
                    required
                    className="InputPassword" />
                <button
                    type="submit"
                    className="LoginButton">LOGIN</button>
            </form>
        </div>
    )
}

export default LoginPage