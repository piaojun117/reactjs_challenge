import React, { PropsWithChildren, useEffect, useState } from 'react'
import NextPrevPageButton from '../component/next_prev_page_button'
import PageButton from '../component/page_button'
import IPage from '../interface/pages'
import UserData from '../interface/user_data'
import * as Constant from '../constant/constant'
import axios from 'axios'
import UserListView from '../component/user_listview'
import UserDetailView from '../component/user_detail_view'

const HomePage: React.FunctionComponent<IPage & PropsWithChildren<any>> = (props) => {


    const [current_page_number, setCurrentPageNumber] = useState<number>(1)
    const [first_page_number, setFirstPageNumber] = useState<number>(1)
    const [page_items, setPageItems] = useState<UserData[]>([])
    const [all_items, setAllItems] = useState<UserData[]>([])
    const [selected_user_data, setSelectedUserData] = useState<UserData>()

    console.log(selected_user_data)

    const fetchData = async (pageNumber: number) => {
        const GRAPH_QUERY = `
        {
            pokemons(first: `+ pageNumber * 10 + `) {
              id
              number
              name
              image
              classification
              weight {
                minimum
                maximum
              }
              height {
                minimum
                maximum
              }
            }
          }
        `;
        // console.log(GRAPH_QUERY)
        const queryResult = await axios.post(Constant.API_URL, { query: GRAPH_QUERY })
        const result = queryResult.data.data.pokemons;
        setPageItems(result)
    }

    useEffect(() => {
        selectPageNumber(1)
    }, [])

    const selectPageNumber = (pageNumber: number) => {
        setCurrentPageNumber(pageNumber)
        fetchData(pageNumber)
    }

    return (
        <div className="HomePageContainer">
            <div className="ListContainer">
                <div className="UserDataListView">
                    <UserListView page_items={page_items} />
                </div>
                <div className="ListPageController">
                    <div className="ButtonGroup">
                        <PageButton onClick={selectPageNumber} page_number={first_page_number} is_selected={current_page_number === first_page_number} />
                        <PageButton onClick={selectPageNumber} page_number={first_page_number + 1} is_selected={current_page_number === first_page_number + 1} />
                        <PageButton onClick={selectPageNumber} page_number={first_page_number + 2} is_selected={current_page_number === first_page_number + 2} />
                        <PageButton onClick={selectPageNumber} page_number={first_page_number + 3} is_selected={current_page_number === first_page_number + 3} />
                    </div>

                    <div className="ButtonGroup">
                        <NextPrevPageButton name="Prev" />
                        <NextPrevPageButton name="Next" />
                    </div>
                </div>
            </div>
            <div className="DetailContainer" >
               <UserDetailView />
            </div>

        </div>
    )
}

export default HomePage