import UserData from '../interface/user_data'
import { SELECT_USER } from './type'

export default function onSelectUser(user_data: UserData) {
    console.log("user_data", user_data)
    return {
        type: SELECT_USER,
        payload: user_data
    }
}