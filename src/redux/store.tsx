
import { createStore } from 'redux'
import { selectUserReducer } from './reducer'

const store = createStore(selectUserReducer)

export default store;