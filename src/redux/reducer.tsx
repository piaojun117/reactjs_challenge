import { SELECT_USER } from './type'
import UserData from '../interface/user_data'

const initialState = {
    user_data: {
        number: "",
        name: ""
    } as UserData,
}

export function selectUserReducer(state = initialState, action: any) {
    console.log("action", action)
    switch (action.type) {
        case SELECT_USER:
            return {
                ...state,
                user_data: action.payload
            }
        default:
            return state
    }
}